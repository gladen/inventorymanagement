package com.tsadev.inventorymanagement;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings.*;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.Toast;

import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.ServerManagedPolicy;


//TODO Add in reports, graph, item categories (user editable), sort options
//TODO Use context menu in InventoryEditor with two options, edit & delete OR use swipe on textview, see yahoo mail for example
//TODO Put button for fullscreen details on item


/*
*
* VERSION MILESTONE FEATURE LIST
*
*/
//TODO V1.0 Initial release
//TODO V1.2 Finish Categories, have reporting started (Feb 2016)
//TODO V2.0 Add in Dropbox/Google Drive support, export options, import options (xml, sqlite, csv, xls) (March 2016)
//TODO V2.5 Barcode scanning(possibly, with retrieval of item data including images, based on barcode) (TBD)

public class MainActivity extends AppCompatActivity {

    TabLayout tabLayout;
    NoSwipeViewPager viewPager;
    Menu actionBarMenu;

    private final static int OPTION_ADD = 10;
    private final static int OPTION_SETTINGS = 20;

    private final static int REQUEST_TUT = 100;
    private final static int REQUEST_EDIT = 200;
    private final static int REQUEST_SETTINGS = 300;

    private LicenseChecker mChecker;
    private LicenseCheckerCallback mLicenseCheckerCallback;
    boolean licensed;
    boolean checkingLicense;
    boolean didCheck;
    private Receiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeVariables();
        tabLayout = (TabLayout)findViewById(R.id.sliding_tabs);
        viewPager = (NoSwipeViewPager)findViewById(R.id.viewpager);
        viewPager.setAdapter(new FragmentAdapter(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setOnTabSelectedListener(new TabSelectListener(viewPager));
        if (Vars.getInstance().firstRun && !Vars.getInstance().version.equals("1")) {
            startActivityForResult(new Intent(this, FirstRun.class), REQUEST_TUT);
            Utils.getDefaultSharedPreferences(this).edit().putBoolean("firstrun", false).apply();
            Utils.getDefaultSharedPreferences(this).edit().putString("version", "1").apply();
            Vars.getInstance().firstRun = false;
            Vars.getInstance().version = "1";
        }
        if (!Vars.getInstance().eulaAccepted) showEULA();
        if (!Vars.getInstance().licensed) checkLicense();
        receiver = new Receiver();
        registerReceiver(receiver, new IntentFilter(Vars.ACTION_EDIT));
    }

    private class Receiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                intent.setAction(null);
                intent.setClass(MainActivity.this, InventoryEdit.class);
                startActivityForResult(intent, REQUEST_EDIT);
            } catch (NullPointerException f) {
                f.printStackTrace();
            } catch(Exception e) {
            }
        }
    }

    private void checkLicense() {
        String deviceId = Secure.getString(getContentResolver(), Secure.ANDROID_ID);
        Log.i("Device Id", deviceId);
        mLicenseCheckerCallback = new LicenseCheck();
        mChecker = new LicenseChecker(this,
                new ServerManagedPolicy(this,
                        new AESObfuscator(Vars.SALT, getPackageName(), deviceId)),
                Vars.BASE64_PUBLIC_KEY);
        doCheck();
    }


    private void initializeVariables() {
        Vars vars = Vars.getInstance();
        SharedPreferences prefs = Utils.getDefaultSharedPreferences(this);
        //vars.useExternalMode = prefs.getBoolean("external", false);
        //vars.useColors = prefs.getBoolean("colors", true);
        //vars.dateFormat = prefs.getString("dateformat", "MM-DD-YYYY");
        vars.firstRun = prefs.getBoolean("firstrun", true);
        vars.eulaAccepted = prefs.getBoolean("eula", false);
        vars.version = prefs.getString("version", "0");
        vars.licensed = prefs.getBoolean("licensed", false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_TUT:
                break;
            case REQUEST_EDIT:
                PageFragment.getInstance().getList().smoothScrollToPosition(0);
                break;
            case REQUEST_SETTINGS:
                if (data != null && data.getAction().equals(Vars.ACTION_RESTART)) {
                        this.finish();
                    }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() != 0) viewPager.setCurrentItem(0);
        else this.finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }



    @SuppressLint("SetJavaScriptEnabled")
    private void showEULA() {
        WebView web = new WebView(this);
        web.getSettings().setJavaScriptEnabled(true);
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setCancelable(false)
                .setView(web)
                .setTitle("End User License Agreement")
                .setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Utils.getDefaultSharedPreferences(MainActivity.this).edit().putBoolean("eula", true).apply();
                        Vars.getInstance().eulaAccepted = true;
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        canceledEULA();
                    }
                })
                .create();
        //dialog.setView(web, 0, 0, 0, 0);
        dialog.show();
        web.loadUrl("file:///android_asset/license.html");
    }

    private void canceledEULA() {
        Toast.makeText(getApplicationContext(), "You must agree to the EULA to use this app.", Toast.LENGTH_LONG).show();
        this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, OPTION_ADD, 0, "Add").setIcon(R.drawable.ic_fa_plus).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        menu.add(0, OPTION_SETTINGS, 10, "Settings").setIcon(R.drawable.ic_fa_gear).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        this.actionBarMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case OPTION_ADD:
                Intent add = new Intent(this, InventoryEdit.class);
                add.putExtra("item", new Item());
                startActivity(add);
                return true;
            case OPTION_SETTINGS:
                startActivityForResult(new Intent(this, SettingsActivity.class), REQUEST_SETTINGS);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class TabSelectListener extends TabLayout.ViewPagerOnTabSelectedListener {

        public TabSelectListener(ViewPager pager) {
            super(pager);
        }

        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            if (tab.getText() != null && tab.getText().toString().equals("Reports")) {
                if (actionBarMenu != null) {
                    actionBarMenu.clear();
                    actionBarMenu.add(0, OPTION_SETTINGS, 0, "Settings")
                            .setIcon(R.drawable.ic_fa_gear)
                            .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                }
                viewPager.setCurrentItem(1);
            }
            else if (tab.getText() != null && tab.getText().toString().equals("Inventory")) {
                if (actionBarMenu != null) {
                    actionBarMenu.clear();
                    actionBarMenu.add(Menu.NONE, OPTION_ADD, 10, "Add")
                            .setIcon(R.drawable.ic_fa_plus)
                            .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                    actionBarMenu.add(Menu.NONE, OPTION_SETTINGS, 20, "Settings")
                            .setIcon(R.drawable.ic_fa_gear)
                            .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                }
                viewPager.setCurrentItem(0);
            }
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {
        }
    }






    private void doCheck() {
        didCheck = false;
        checkingLicense = true;
        mChecker.checkAccess(mLicenseCheckerCallback);
    }



    private class LicenseCheck implements LicenseCheckerCallback {

        @Override
        public void allow(int reason) {
            if (isFinishing()) {
                return;
            }
            Log.i("License","Accepted!");

            licensed = true;
            checkingLicense = false;
            didCheck = true;
            Utils.getDefaultSharedPreferences(MainActivity.this).edit().putBoolean("licensed", true).apply();
            Vars.getInstance().licensed = true;
        }

        @SuppressWarnings("deprecation")
        @Override
        public void dontAllow(int reason) {
            if (isFinishing()) {
                return;
            }
            String realReason = "";
            if (reason == 561) realReason = "Not Licensed";
            Log.i("License","Denied!");
            Log.i("License","Reason for denial: " + realReason);

            licensed = false;
            checkingLicense = false;
            didCheck = true;

            if (Vars.CHECK_LICENSE) showDialog(0);

        }

        @SuppressWarnings("deprecation")
        @Override
        public void applicationError(int reason) {
            Log.i("License", "Error: " + reason);
            if (isFinishing()) {
                return;
            }
            licensed = true;
            checkingLicense = false;
            didCheck = false;

            if (Vars.CHECK_LICENSE) showDialog(0);
        }


    }

    protected Dialog onCreateDialog(int id) {
        // We have only one dialog.
        return new AlertDialog.Builder(this)
                .setTitle("UNLICENSED APPLICATION")
                .setMessage("This application is not licensed, please buy it from the Play Store.")
                .setPositiveButton("Buy", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(
                                "http://market.android.com/details?id=" + getPackageName()));
                        startActivity(marketIntent);
                        finish();
                    }
                })
                .setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNeutralButton("Re-Check", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        doCheck();
                    }
                })

                .setCancelable(false)
                .setOnKeyListener(new DialogInterface.OnKeyListener(){
                    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                        Log.i("License", "Key Listener");
                        finish();
                        return true;
                    }
                })
                .create();

    }
}
