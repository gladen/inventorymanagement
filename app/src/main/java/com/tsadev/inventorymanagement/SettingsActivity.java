package com.tsadev.inventorymanagement;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SettingsActivity extends AppCompatPreferenceActivity {

    //TODO Allow switching on/off for colors

    @SuppressLint("SimpleDateFormat")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PreferenceScreen root = getPreferenceManager().createPreferenceScreen(this);
        PreferenceCategory head = new PreferenceCategory(this);
        head.setTitle("Settings");
        root.addPreference(head);

        Preference clearDB = new Preference(this);
        clearDB.setTitle("Clear Inventory");
        clearDB.setSummary("Removes all items from your inventory");
        clearDB.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog.Builder alert = new AlertDialog.Builder(SettingsActivity.this);
                alert.setTitle("Warning");
                alert.setMessage("This action is irreversible, are you sure you want to do this?");
                alert.setCancelable(false);
                alert.setPositiveButton("Okay", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SettingsActivity.this.deleteDatabase("inventory.sqlite");
                        Toast.makeText(SettingsActivity.this, "Application will quit, please restart app", Toast.LENGTH_SHORT).show();
                        setResult(RESULT_OK, new Intent().setAction(Vars.ACTION_RESTART));
                        SettingsActivity.this.finish();
                    }
                });
                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog = alert.create();
                dialog.show();
                return true;
            }
        });
        head.addPreference(clearDB);


        if (Vars.DEBUG) {

            /*
            CheckBoxPreference externDB = new CheckBoxPreference(this);
            externDB.setTitle("External Databases");
            externDB.setSummaryOff("Use downloaded external databases");
            externDB.setSummaryOn("Disable use of external databases");
            externDB.setKey("external");
            externDB.setDefaultValue(false);
            externDB.setPersistent(true);
            head.addPreference(externDB);
            */

            CheckBoxPreference useColors = new CheckBoxPreference(this);
            useColors.setTitle("Use Colors for Categories");
            useColors.setSummaryOn("Using color labels");
            useColors.setSummaryOff("Not using color labels");
            useColors.setKey("colors");
            useColors.setDefaultValue(true);
            useColors.setPersistent(true);
            head.addPreference(useColors);


            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, 0);
            Date date = cal.getTime();
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-dd-MM");
            SimpleDateFormat dateFormat3 = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat dateFormat4 = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat dateFormat5 = new SimpleDateFormat("dd-MM-yy");
            SimpleDateFormat dateFormat6 = new SimpleDateFormat("MM-dd-yy");

            String[] formats = new String[] {
                    dateFormat1.format(date) + " (YYYY-MM-DD)",
                    dateFormat2.format(date) + " (YYYY-DD-MM)",
                    dateFormat3.format(date) + " (DD-MM-YYYY)",
                    dateFormat4.format(date) + " (MM-DD-YYYY)", //And just to make it more confusing
                    dateFormat5.format(date) + " (DD-MM-YY)",
                    dateFormat6.format(date) + " (MM-DD-YY)"
            };

            String[] formatValues = new String[] {
                    "yyyy-MM-dd",
                    "yyyy-dd-MM",
                    "dd-MM-yyyy",
                    "MM-dd-yyyy",
                    "dd-MM-yy",
                    "MM-dd-yy"
            };

            final ListPreference dateFormat = new ListPreference(this);
            dateFormat.setEntries(formats);
            dateFormat.setEntryValues(formatValues);
            if (dateFormat.getValue() == null) dateFormat.setValue("yyyy-MM-dd");
            dateFormat.setTitle("Date Format");
            dateFormat.setSummary("Date format for reports. Currently: " + dateFormat.getValue().toUpperCase());
            dateFormat.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String value = (String) newValue;
                    dateFormat.setSummary("Date format for reports. Currently: " + value.toUpperCase());
                    return true;
                }
            });
            dateFormat.setDialogTitle("Set Date Format:");
            dateFormat.setPersistent(true);
            dateFormat.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    dateFormat.getDialog().setCancelable(false);
                    return false;
                }
            });
            head.addPreference(dateFormat);


            PreferenceCategory debug = new PreferenceCategory(this);
            debug.setTitle("Debug");
            root.addPreference(debug);
            Preference clearPrefs = new Preference(this);
            clearPrefs.setTitle("Clear SharedPreferences");
            clearPrefs.setSummary("Clears the SharedPreferences");
            clearPrefs.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Utils.getDefaultSharedPreferences(SettingsActivity.this).edit().clear().apply();
                    setResult(RESULT_OK, new Intent().setAction(Vars.ACTION_RESTART));
                    SettingsActivity.this.finish();
                    return true;
                }
            });
            debug.addPreference(clearPrefs);
        }

        setPreferenceScreen(root);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        this.finish();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
