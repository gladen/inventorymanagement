package com.tsadev.inventorymanagement;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

import java.text.NumberFormat;

public class CurrencyEditText extends EditText {

    public CurrencyEditText(Context context) {
        super(context);
        this.addTextChangedListener(new CurrencyTextWatcher(this));
    }

    public CurrencyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.addTextChangedListener(new CurrencyTextWatcher(this));
    }

    public CurrencyEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.addTextChangedListener(new CurrencyTextWatcher(this));
    }

    @Override
    public void onSelectionChanged(int start, int end) {
        CharSequence text = getText();
        if (text != null) {
            if (start != text.length() || end != text.length()) {
                setSelection(text.length(), text.length());
                return;
            }
        }

        super.onSelectionChanged(start, end);
    }

    private class CurrencyTextWatcher implements TextWatcher {

        private EditText edit;
        private String past;

        CurrencyTextWatcher(EditText edit) {
            this.edit = edit;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            past = s.toString();
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!s.toString().isEmpty()) {
                edit.removeTextChangedListener(this);
                String cleanString = s.toString().replaceAll("[$,.]", "");
                if (cleanString.length() < 6) {
                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance().format((parsed / 100));
                    edit.setText(formatted);
                } else {
                    edit.setText(past);
                }
                edit.addTextChangedListener(this);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
}
