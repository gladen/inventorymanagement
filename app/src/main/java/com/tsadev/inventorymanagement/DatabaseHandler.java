package com.tsadev.inventorymanagement;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    // Static Variables for database manipulation
    private static final String DB_NAME = "inventory.sqlite";
    private static final int DB_VERSION = 1;
    private static final String TABLE_NAME = "inventory";
    private static final String KEY_ID = "id";
    private static final String KEY_ITEM_NAME = "name";
    private static final String KEY_DESC = "description";
    private static final String KEY_COUNT = "count";
    private static final String KEY_COLOR = "color";
    private static final String KEY_CATEGORY = "category";
    private static final String KEY_BARCODE = "barcode";
    private static final String KEY_REASON = "reason";
    private static final String KEY_PRICE = "price";



    public DatabaseHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_ITEM_NAME + " TEXT UNIQUE,"
                + KEY_DESC + " TEXT,"
                + KEY_COLOR + " TEXT,"
                + KEY_CATEGORY + " TEXT,"
                + KEY_COUNT + " INTEGER,"
                + KEY_BARCODE + " TEXT,"
                + KEY_REASON + " TEXT,"
                + KEY_PRICE + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

    // CRUD operations

    public synchronized long addItem(Item item) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ITEM_NAME, item.getName());
        values.put(KEY_DESC, item.getDesc());
        values.put(KEY_COLOR, item.getColor());
        values.put(KEY_CATEGORY, item.getCategory());
        values.put(KEY_COUNT, item.getCount());
        values.put(KEY_BARCODE, item.getBarcode());
        values.put(KEY_REASON, item.getReason());
        values.put(KEY_PRICE, item.getPrice());
        return db.insert(TABLE_NAME, null, values);
    }

    public synchronized boolean updateItem(Item item) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ITEM_NAME, item.getName());
        values.put(KEY_DESC, item.getDesc());
        values.put(KEY_COLOR, item.getColor());
        values.put(KEY_CATEGORY, item.getCategory());
        values.put(KEY_COUNT, item.getCount());
        values.put(KEY_BARCODE, item.getBarcode());
        values.put(KEY_REASON, item.getReason());
        values.put(KEY_PRICE, item.getPrice());
        int result = 0;
        try {
            result = db.update(TABLE_NAME, values, KEY_ID + " = ?", new String[] { String.valueOf(item.getId()) });
        } catch (SQLiteException e) {
            //Report to activity or return different value on error
        }
        return result > 0;
    }

    public synchronized Item getItem(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME, new String[]{KEY_ID,
                        KEY_ITEM_NAME, KEY_DESC, KEY_COLOR, KEY_CATEGORY, KEY_COUNT, KEY_BARCODE, KEY_REASON, KEY_PRICE}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            Item item = new Item();
            item.setId(cursor.getInt(0));
            item.setName(cursor.getString(1));
            item.setDesc(cursor.getString(2));
            item.setColor(cursor.getString(3));
            item.setCategory(cursor.getString(4));
            item.setCount(cursor.getInt(5));
            item.setBarcode(cursor.getString(6));
            item.setReason(cursor.getString(7));
            item.setPrice(cursor.getString(8));
            cursor.close();
            return item;
        }
        // Cursor was null, error?
        else return null;

    }

    public synchronized boolean deleteItem(Item item) {
        SQLiteDatabase db = this.getWritableDatabase();
        int result = db. delete(TABLE_NAME, KEY_ID + " = ?",
                new String[]{String.valueOf(item.getId())});
        return result > 0;
    }

    public synchronized List<Item> getAll() {
        List<Item> result = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                int id = 0, count = 0;
                String name="",desc="",color="",category="",barcode="",reason="",price="";
                try {
                    id = cursor.getInt(0);
                    name = cursor.getString(1);
                    desc = cursor.getString(2);
                    color = cursor.getString(3);
                    category = cursor.getString(4);
                    count = cursor.getInt(5);
                    barcode = cursor.getString(6);
                    reason = cursor.getString(7);
                    price = cursor.getString(8);
                } catch (IllegalStateException e) {
                    //Throw error to activity
                } finally {
                    Item item = new Item();
                    item.setId(id);
                    item.setName(name);
                    item.setDesc(desc);
                    item.setColor(color);
                    item.setCategory(category);
                    item.setCount(count);
                    item.setBarcode(barcode);
                    item.setReason(reason);
                    item.setPrice(price);
                    result.add(item);
                }


            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }

    public synchronized int getTotalCount() {
        int result = 0;

        String selectQuery = "SELECT * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                int count = cursor.getInt(5);
                result += count;
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }
}
