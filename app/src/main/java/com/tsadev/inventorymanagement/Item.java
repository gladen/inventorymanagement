package com.tsadev.inventorymanagement;

import java.io.Serializable;
import java.text.NumberFormat;

public class Item implements Serializable {

    private String name, desc, color, category, barcode, reason, price, priceAll = "";
    private int count;
    private long id = -1;

    public Item() {

    }

    public Item(String name, String color, int count) {
        this.name = name;
        this.color = color;
        this.count = count;
    }

    public Item(String name, String desc, String color) {
        this.name = name;
        this.desc = desc;
        this.color = color;
        this.count = 0;
    }

    public Item(String name, String desc, String color, int count) {
        this.name = name;
        this.desc = desc;
        this.color = color;
        this.count = count;
    }



    public String getName() { return this.name; }

    public String getDesc() { return this.desc; }

    public String getColor() { return this.color; }

    public String getCategory() { return this.category; }

    public int getCount() { return this.count; }

    public long getId() { return this.id; }

    public String getBarcode() { return this.barcode; }

    public String getReason() { return this.reason; }

    public String getPrice() { return this.price; }

    public String getPriceAll() {
        return priceAll;
    }

    public void setName(String name) { this.name = name; }

    public void setDesc(String desc) { this.desc = desc; }

    public void setColor(String color) { this.color = color; }

    public void setCategory(String category) { this.category = category; }

    public void setCount(int count) { this.count = count; }

    public void setId(long id) { this.id = id; }

    public void setBarcode(String barcode) { this.barcode = barcode; }

    public void setReason(String reason) { this.reason = reason; }

    public void setPrice(String price) {
        this.price = price;
        if (price != null && !price.isEmpty())
            this.priceAll = NumberFormat
                    .getCurrencyInstance()
                    .format((Double.parseDouble(price.replaceAll("[$,.]", "")) * (double) count) / 100);
    }
}
