package com.tsadev.inventorymanagement;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.tsadev.tutorialscreen.Tutorial;
import com.tsadev.tutorialscreen.TutorialFragment;

import java.util.ArrayList;
import java.util.List;

//TODO Run FirstRun and present EULA (assets/license.html)

public class FirstRun extends Tutorial {

    Tutorial.PageAdapter adapter;
    ViewPager pager;

    @Override
    public void onInit() {
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(TutorialFragment.newInstance("Page 1"));
        fragments.add(TutorialFragment.newInstance("Page 2"));
        fragments.add(TutorialFragment.newInstance("Page 3"));
        setFragments(fragments);
    }

    @Override
    public void onCreate(){
        pager = getViewPager();
        adapter = getPageAdapter();
    }

    @Override
    public void onNextClick(int position){

    }

    @Override
    public void onDoneClick() {
        setResult(RESULT_OK);
        this.finish();
    }

    @Override
    public void onSkipClick() {
        setResult(RESULT_OK);
        this.finish();
    }

    @Override
    public void onBackOut() {
        setResult(RESULT_CANCELED);
        this.finish();
    }
}