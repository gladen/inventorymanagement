package com.tsadev.inventorymanagement;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CurrentInvAdapter extends BaseAdapter {

    private Context context;
    private List<Item> items = new ArrayList<>();
    private LayoutInflater inflater;

    public CurrentInvAdapter(Context context, List<Item> items) {
        inflater = LayoutInflater.from(context);
        this.items = items;
        this.context = context;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Item getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Item item = items.get(position);
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_row_item, parent, false);
            holder = new Holder();
            holder.name = (TextView)convertView.findViewById(R.id.item_title);
            holder.count = (TextView)convertView.findViewById(R.id.item_subtitle);
            holder.color = (ImageView)convertView.findViewById(R.id.item_color);
            convertView.setTag(holder);
        }
        else {
            holder = (Holder) convertView.getTag();
        }
        holder.name.setText(item.getName());
        holder.count.setText(String.valueOf(item.getCount()));
        String itemColor = item.getColor();
        holder.color.setColorFilter(0);
        if (itemColor != null && !itemColor.isEmpty()) {
            try {
                int color = Color.parseColor(itemColor);
                holder.color.setColorFilter(color);
            } catch (IllegalArgumentException e) {
                Log.e("TSDEBUG", "Couldn't parse color");
            }
        } else {
            holder.color.setColorFilter(0xFFcfcfcf);
        }

        return convertView;
    }


    private class Holder {
        Item item;
        ImageView color;
        TextView name, count;
    }
}