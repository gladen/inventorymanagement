package com.tsadev.inventorymanagement;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

public class PageFragment extends Fragment{


    public static PageFragment fragment;

    private ItemAdapter listAdapter;
    private ExpandableListView listView;
    private List<Item> itemList;
    private DatabaseHandler db;
    private boolean startup = true;

    public static PageFragment getInstance() {
        if (fragment == null) {
            synchronized (PageFragment.class) {
                if (fragment == null) {
                    fragment = new PageFragment();
                }
            }
        }
        return fragment;
    }

    public ExpandableListView getList(){
        return listView;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return createLayout();
    }


    private View createLayout() {
        LinearLayout invRoot = new LinearLayout(getActivity());
        invRoot.setOrientation(LinearLayout.VERTICAL);
        invRoot.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        listView = new ExpandableListView(getActivity());
        listView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 1));
        db = new DatabaseHandler(getActivity());
        itemList = db.getAll();
        invRoot.addView(listView);
        listAdapter = new ItemAdapter(getActivity(), itemList);
        listView.setAdapter(listAdapter);
        return invRoot;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (startup) {
            startup = false;
        } else {
            itemList.clear();
            itemList.addAll(db.getAll());
            listAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Parcelable listState = listView.onSaveInstanceState();
        outState.putParcelable("ListState", listState);
    }


    @Override
    public void onViewStateRestored(Bundle state) {
        super.onViewStateRestored(state);
        if (state != null) {
            Parcelable listState = state.getParcelable("ListState");
            if (listState != null) {
                listView.onRestoreInstanceState(listState);
            }
        }
    }
}
