package com.tsadev.inventorymanagement;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.widget.ProgressBar;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class DatabaseDownloader extends AsyncTask<String, Integer, Void> {

    private Context context;
    private ProgressBar progressBar;

    public DatabaseDownloader (Context context, @Nullable ProgressBar progressBar) {
        this.context = context;
        this.progressBar = progressBar;
    }

    @Override
    protected Void doInBackground(String... f_url) {
        int count;
        try {
            URL url = new URL(f_url[0]);
            URLConnection connection = url.openConnection();
            connection.connect();
            int lengthOfFile = connection.getContentLength();
            InputStream input = new BufferedInputStream(url.openStream(), 8192);
            OutputStream output = new FileOutputStream(context.getFilesDir() + url.getFile());
            byte data[] = new byte[1024];
            long total = 0;
            while ((count = input.read(data)) != -1) {
                total += count;
                publishProgress((int)(total * 100) / lengthOfFile);
                output.write(data, 0, count);
            }
            output.flush();
            output.close();
            input.close();

        } catch (IOException e) {
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        if (progressBar != null) progressBar.setProgress(progress[0]);
    }

    @Override
    protected void onPostExecute(Void empty) {
    }
}
