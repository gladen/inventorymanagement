package com.tsadev.inventorymanagement;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.text.InputFilter;
import android.text.InputType;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.concurrent.atomic.AtomicInteger;

public class Utils {

    /**
     * Method for getting the available screen size of the display, (hopefully) minus the status and/or nav bars
     * @param display the display to get size details from
     * @return an integer array containing the width, height, and recommended offset to center the view
     */

    @SuppressWarnings("deprecation")
    public static int[] getScreenSize(Display display) {
        int width = display.getWidth();
        int height = display.getHeight();
        int wwidth;
        int wheight;
        int offset;
        if (width > height) {
            if ((height * 0.5) > 100) {
                wheight = (int) (width * 0.4);
                wwidth = (int) (height * 0.8);
            }
            else {
                wheight = FrameLayout.LayoutParams.WRAP_CONTENT;
                wwidth = FrameLayout.LayoutParams.WRAP_CONTENT;
            }
            offset = height/2;
        }
        else if (height > width) {
            if ((height * 0.5) > 100) {
                wheight = (int) (height * 0.4);
                wwidth = (int) (width * 0.8);
            }
            else {
                wheight = FrameLayout.LayoutParams.WRAP_CONTENT;
                wwidth = FrameLayout.LayoutParams.WRAP_CONTENT;
            }
            offset = width/10;
        }
        else {
            wheight = FrameLayout.LayoutParams.WRAP_CONTENT;
            wwidth = FrameLayout.LayoutParams.WRAP_CONTENT;
            offset = 120;
        }

        return new int[] {wwidth, wheight, offset};
    }

    /**
     * Generates a randomized ID for views
     * @return the newly created ID
     */

    public static int generateViewId() {
        final AtomicInteger sNextGeneratedId = new AtomicInteger(1);
        for (;;) {
            final int result = sNextGeneratedId.get();
            // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
            int newValue = result + 1;
            if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
            if (sNextGeneratedId.compareAndSet(result, newValue)) {
                return result;
            }
        }
    }

    public static SharedPreferences getDefaultSharedPreferences(Context context) {
        return context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE);
    }


    /**
     * Creates a popup window with the given parameters
     * @param dialog The view to inflate
     * @param width the width of the popup window
     * @param height the height of the popup window
     * @param offset the offset to center the window
     * @param root the root view to attach to
     * @param context the activity's context
     * @return the popup window created, so that you may call dismiss() on it later
     */

    public static PopupWindow createPopup(View dialog, int width, int height, int offset, View root, Context context) {
        PopupWindow popup = new PopupWindow(dialog, width, height);
        popup.setInputMethodMode(PopupWindow.INPUT_METHOD_FROM_FOCUSABLE);
        popup.setFocusable(true);
        popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_UNSPECIFIED);
        popup.showAsDropDown(root, offset, 0);
        View container = popup.getContentView();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams p = (WindowManager.LayoutParams) container.getLayoutParams();
        p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        p.dimAmount = 0.5f;
        wm.updateViewLayout(container, p);
        return popup;
    }

    public static void showAddDialog(Context context) {
        LinearLayout root = new LinearLayout(context);
        root.setOrientation(LinearLayout.VERTICAL);
        root.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));



        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("Add Item");
        alert.setView(root);
        alert.setCancelable(false);
        alert.setPositiveButton("Save", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

    public static void showEditDialog(Context context, EditView view,
                                      DialogInterface.OnClickListener positive, DialogInterface.OnClickListener neutral) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("Edit Item");
        alert.setView(view);
        alert.setCancelable(false);
        alert.setPositiveButton("Save", positive);
        alert.setNeutralButton("Delete", neutral);
        alert.setNegativeButton("Cancel", null);
        AlertDialog dialog = alert.create();
        dialog.show();
    }


    /*
    *
    *       VIEWS FOR POPUP WINDOWS
    *
     */

    public static class EditView extends RelativeLayout {

        public EditText name,count;

        public Button color;

        public ImageView colorPrev;

        @SuppressLint("SetTextI18n")
        public EditView(Context context) {
            super(context);
            setPadding(10, 10, 10, 10);
            setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            LayoutParams editParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            LayoutParams countDescParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            LayoutParams countParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            LayoutParams colorDescParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            LayoutParams colorPrevParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            LayoutParams colorParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

            editParams.addRule(ALIGN_PARENT_TOP, TRUE);
            LinearLayout editControls = new LinearLayout(context);
            editControls.setOrientation(LinearLayout.VERTICAL);
            editControls.setPadding(30, 30, 30, 0);
            editControls.setLayoutParams(editParams);

            TextView summary = new TextView(context);
            summary.setText("Enter item details:");
            editControls.addView(summary);

            name = new EditText(context);
            name.setHint("Enter item name");
            name.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
            editControls.addView(name);

            RelativeLayout countContainer = new RelativeLayout(context);
            countContainer.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT));

            countDescParams.addRule(ALIGN_PARENT_LEFT, TRUE);
            countDescParams.addRule(CENTER_VERTICAL, TRUE);
            TextView countDesc = new TextView(context);
            countDesc.setText("Enter # of items:");
            countDesc.setLayoutParams(countDescParams);
            countContainer.addView(countDesc);

            countParams.addRule(ALIGN_PARENT_RIGHT, TRUE);
            count = new EditText(context);
            count.setHint("0");
            count.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});
            count.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
            count.setLayoutParams(countParams);
            countContainer.addView(count);

            editControls.addView(countContainer);

            RelativeLayout colorContainer = new RelativeLayout(context);
            colorContainer.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));


            TextView colorDesc = new TextView(context);
            color = new Button(context);
            colorPrev = new ImageView(context);
            colorPrev.setId(Utils.generateViewId());

            colorDescParams.addRule(ALIGN_PARENT_LEFT, TRUE);
            colorDescParams.addRule(CENTER_VERTICAL, TRUE);
            colorDesc.setText("Select color:");
            colorDesc.setLayoutParams(colorDescParams);
            colorContainer.addView(colorDesc);

            colorParams.addRule(LEFT_OF, colorPrev.getId());
            color.setText("SELECT");
            color.setLayoutParams(colorParams);
            colorContainer.addView(color);

            colorPrevParams.addRule(ALIGN_PARENT_RIGHT, TRUE);
            colorPrev.setImageResource(R.drawable.nocolor);
            colorPrev.setLayoutParams(colorPrevParams);
            colorContainer.addView(colorPrev);

            editControls.addView(colorContainer);
            addView(editControls);
        }

        public EditText getName() { return name; }
        public EditText getCount() { return count; }
        public Button getColor() { return color; }
        public ImageView getColorPreview() { return colorPrev; }
    }
}
