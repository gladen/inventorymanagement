package com.tsadev.inventorymanagement;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class InventoryEdit extends AppCompatActivity {

    private static final int OPTION_SAVE = 10;
    private static final int OPTION_DELETE = 20;
    private Item item;
    EditText name, desc, count, price;
    Spinner cat;
    private boolean delete = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);
        if (getIntent() != null) {
            if (getIntent().getSerializableExtra("item") != null) {
                item = (Item) getIntent().getSerializableExtra("item");
            }
        }
        if (item == null) {
            Toast.makeText(this, "Error, please try again", Toast.LENGTH_LONG).show();
            this.finish();
            return;
        }
        name = (EditText)findViewById(R.id.name);
        desc = (EditText)findViewById(R.id.desc);
        cat = (Spinner)findViewById(R.id.cat);
        count = (EditText)findViewById(R.id.count);
        price = (EditText)findViewById(R.id.price);
        name.setText(item.getName());
        desc.setText(item.getDesc());
        count.setText(String.valueOf(item.getCount()));
        if (item.getPrice() != null && !item.getPrice().isEmpty()) price.setText(item.getPrice());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, OPTION_SAVE, 0, "Save").setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        if (this.item.getId() != -1) menu.add(0, OPTION_DELETE, 10, "Delete");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final DatabaseHandler db = new DatabaseHandler(this);
        switch (item.getItemId()) {
            case OPTION_SAVE:
                this.item.setName(name.getText().toString());
                this.item.setDesc(desc.getText().toString());
                this.item.setCount(Integer.parseInt(count.getText().toString()));
                this.item.setPrice(price.getText().toString());
                if (this.item.getId() != -1) db.updateItem(this.item);
                else db.addItem(this.item);
                this.finish();
                break;
            case OPTION_DELETE:
                this.delete = true;
                SpannableStringBuilder snackbarText = new SpannableStringBuilder();
                snackbarText.append("Item deleting...");
                snackbarText.setSpan(new ForegroundColorSpan(0xFFFF4747),
                        0, snackbarText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                snackbarText.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),
                        0, snackbarText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                Snackbar
                        .make(name, snackbarText, Snackbar.LENGTH_LONG)
                        .setAction("STOP", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                delete = false;
                            }
                        })
                        .setCallback(new Snackbar.Callback() {
                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                super.onDismissed(snackbar, event);
                                if (delete) {
                                    db.deleteItem(InventoryEdit.this.item);
                                    InventoryEdit.this.finish();
                                }
                            }
                        })
                        .show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
