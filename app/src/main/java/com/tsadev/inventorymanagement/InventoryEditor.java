package com.tsadev.inventorymanagement;

import android.animation.TimeInterpolator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.tsadev.androidcolorpicker.ColorPickerDialog;

import java.util.List;

public class InventoryEditor extends AppCompatActivity {

    private static final int OPTION_ADD = 10;
    private static final int OPTION_DONE = 20;
    private static final int textview_id = Utils.generateViewId();


    private static final TimeInterpolator sDecelerator = new DecelerateInterpolator();
    private static final TimeInterpolator sAccelerator = new AccelerateInterpolator();
    private int mLeftDelta;
    private int mTopDelta;
    private static final int ANIM_DURATION = 250;

    private CurrentInvAdapter listAdapter;
    private List<Item> itemList;
    private ListView listView;
    private DatabaseHandler db;

    private PopupWindow popup;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(createLayout());
        listView.setOnItemClickListener(new OnListItemClick());

        Bundle bundle = getIntent().getExtras();
        final int listTop = bundle.getInt(getPackageName() + ".top");
        final int listLeft = bundle.getInt(getPackageName() + ".left");

        if (savedInstanceState == null) {
            ViewTreeObserver observer = listView.getViewTreeObserver();
            observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

                @Override
                public boolean onPreDraw() {
                    listView.getViewTreeObserver().removeOnPreDrawListener(this);

                    int[] screenLocation = new int[2];
                    listView.getLocationOnScreen(screenLocation);
                    mLeftDelta = listLeft + screenLocation[0];
                    mTopDelta = listTop + screenLocation[1];
                    runEnterAnimation();
                    return true;
                }
            });
        }
    }

    private class ColorHolder {
        private String color;
        public String getColor() { return color; }
        public void setColor(String color) {this.color = color;}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, OPTION_ADD, 10, "Add").setIcon(R.drawable.ic_fa_plus).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        menu.add(0, OPTION_DONE, 20, "Done").setIcon(R.drawable.ic_fa_check).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case OPTION_ADD:
                addItem();
                return true;
            case OPTION_DONE:
                runExitAnimation(new Runnable() {
                    public void run() {
                        finish();
                    }
                });
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * The enter animation scales the picture in from its previous thumbnail
     * size/location, colorizing it in parallel. In parallel, the background of the
     * activity is fading in. When the pictue is in place, the text description
     * drops down.
     */
    public void runEnterAnimation() {
        final long duration = (long) (ANIM_DURATION);
        listView.setPivotX(0);
        listView.setPivotY(0);
        listView.setTranslationX(mLeftDelta);
        listView.setTranslationY(mTopDelta);
        listView.animate().setDuration(duration).translationY(0).
                setInterpolator(sDecelerator);
    }

    public void runExitAnimation(final Runnable endAction) {
        final long duration = (long) (ANIM_DURATION);
        listView.animate()
                .translationX(mLeftDelta)
                .translationY(2000)
                .setDuration(duration/2)
                .setInterpolator(sAccelerator)
                .withEndAction(endAction);
    }

    @Override
    public void onBackPressed() {
        runExitAnimation(new Runnable() {
            public void run() {
                finish();
            }
        });
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }


    @SuppressLint("InflateParams")
    private void addItem() {
        int[] tmp = Utils.getScreenSize(getWindowManager().getDefaultDisplay());
        final int wwidth = tmp[0];
        final int wheight = tmp[1] + 140;
        final int offset = tmp[2];
        //noinspection UnusedAssignment
        tmp = null;
        final boolean external;
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView;
        if (Vars.getInstance().useExternalMode) {
            dialogView = inflater.inflate(R.layout.extern_add_item, null);
            external = true;
        }
        else {
            dialogView = inflater.inflate(R.layout.local_add_item, null);
            external = false;
        }
        Button colorButton = (Button)dialogView.findViewById(R.id.colorSelect);
        TextView cancel = (TextView)dialogView.findViewById(R.id.cancel_item_Button);
        TextView add = (TextView)dialogView.findViewById(R.id.add_item_button);
        final ImageView colorPrev = (ImageView)dialogView.findViewById(R.id.colorPrev);
        final ColorHolder color = new ColorHolder();
        colorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ColorPickerDialog dialog = new ColorPickerDialog(InventoryEditor.this, 0xFFFFFFFF,
                        new ColorPickerDialog.OnColorPickerListener() {
                            @Override
                            public void onCancel(ColorPickerDialog dialog) {
                                // Do Nothing
                            }

                            @Override
                            public void onOk(ColorPickerDialog dialog, int chosenColor) {
                                color.setColor("#" + Integer.toHexString(chosenColor));
                                colorPrev.setImageResource(R.drawable.color_blank);
                                colorPrev.setColorFilter(chosenColor);
                            }
                        });
                dialog.getDialog().setCancelable(false);
                dialog.show();
            }
        });
        popup = Utils.createPopup(dialogView, wwidth, wheight, offset, findViewById(textview_id), InventoryEditor.this);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText name;
                if (external) {
                    name = (AutoCompleteTextView) dialogView.findViewById(R.id.externItemName);
                } else {
                    name = (EditText) dialogView.findViewById(R.id.localItemName);
                }
                EditText count = (EditText) dialogView.findViewById(R.id.count);
                String itemName = name.getText().toString();
                String countTemp = count.getText().toString();
                if (itemName.equals("") || countTemp.equals("")) {
                    Toast.makeText(InventoryEditor.this, "Please fill both fields", Toast.LENGTH_SHORT).show();
                    return;
                }

                int itemCount = Integer.parseInt(countTemp);

                List<Item> items = db.getAll();
                for (Item itemTmp : items) {
                    if (itemTmp.getName().equals(itemName)) {
                        Toast.makeText(InventoryEditor.this, "Item exists", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }


                Item item = new Item(itemName, color.getColor(), itemCount);
                //Modify DB and List simultaneously then tell adapter that data has changed
                long result = db.addItem(item);
                if (result == -1) {
                    Log.e("TSDEBUG", "Error writing item to database");
                }
                else item.setId(result);
                itemList.add(item);
                listAdapter.notifyDataSetChanged();
                popup.dismiss();
            }
        });


    }

    @SuppressLint("SetTextI18n")
    private View createLayout() {
        LinearLayout root = new LinearLayout(this);
        root.setOrientation(LinearLayout.VERTICAL);
        root.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));

        TextView tapText = new TextView(this);
        tapText.setText("Tap on an item to edit");
        tapText.setGravity(Gravity.CENTER_HORIZONTAL);
        tapText.setId(textview_id);
        tapText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        root.addView(tapText);

        View border = new View(this);
        border.setBackgroundColor(0x00000000);
        border.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 5));
        root.addView(border);

        listView = new ListView(this);
        listView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 1));
        root.addView(listView);
        db = new DatabaseHandler(this);
        itemList = db.getAll();
        listAdapter = new CurrentInvAdapter(this, itemList);
        listView.setAdapter(listAdapter);

        return root;
    }

    private class OnListItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) {
            final Item item = (Item) parent.getItemAtPosition(position);
            int[] tmp = Utils.getScreenSize(getWindowManager().getDefaultDisplay());
            final int wwidth = tmp[0];
            final int wheight = tmp[1] + 100;
            final int offset = tmp[2];
            //noinspection UnusedAssignment
            tmp = null;
            Utils.EditView editView = new Utils.EditView(InventoryEditor.this);
            final EditText name = editView.getName();
            final EditText count = editView.getCount();
            final Button colorButton = editView.getColor();
            final ImageView colorPrev = editView.getColorPreview();
            final ColorHolder color = new ColorHolder();
            final int initialColor;
            if (item.getColor() != null && !item.getColor().equals("")) {
                initialColor = Color.parseColor(item.getColor());
                colorPrev.setImageResource(R.drawable.color_blank);
                colorPrev.setColorFilter(initialColor);
            } else initialColor = 0xFFFFFFFF;
            colorButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ColorPickerDialog dialog = new ColorPickerDialog(InventoryEditor.this, initialColor,
                            new ColorPickerDialog.OnColorPickerListener() {
                                @Override
                                public void onCancel(ColorPickerDialog dialog) {
                                    // Do Nothing
                                }

                                @Override
                                public void onOk(ColorPickerDialog dialog, int chosenColor) {
                                    color.setColor("#" + Integer.toHexString(chosenColor));
                                    colorPrev.setImageResource(R.drawable.color_blank);
                                    colorPrev.setColorFilter(chosenColor);
                                }
                            });
                    dialog.getDialog().setCancelable(false);
                    dialog.show();
                }
            });
            name.setText(item.getName());
            count.setText(String.valueOf(item.getCount()));
        }
    }

    {
        /*

        //Delete
        AlertDialog.Builder alert = new AlertDialog.Builder(v.getContext());
        alert.setTitle("Warning");
        alert.setMessage("This action is irreversible, are you sure you want to do this?");
        alert.setCancelable(false);
        alert.setPositiveButton("Okay", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                removeItem(view, item);
                dialog.dismiss();
                popup_edit.dismiss();
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = alert.create();
        dialog.show();

        //Save
        item.setName(name.getText().toString());
        item.setCount(Integer.parseInt(count.getText().toString()));
        item.setColor(color.getColor());
        db.updateItem(item);
        itemList.set(position, item);
        listAdapter.notifyDataSetChanged();
        popup_edit.dismiss();
        */
    }

    private void removeItem(View row, final Item item) {
        final Animation animation = AnimationUtils.loadAnimation(InventoryEditor.this, android.R.anim.slide_out_right);
        row.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                db.deleteItem(item);
                if (!itemList.remove(item)) {
                    Log.e("TSDEBUG", "Error removing item from list");
                }
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
}