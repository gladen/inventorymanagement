package com.tsadev.inventorymanagement;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.PreferenceFragmentCompat;

public class ReportsFragment extends PreferenceFragmentCompat {

    public static ReportsFragment fragment;

    public static ReportsFragment getInstance() {
        if (fragment == null) {
            fragment = new ReportsFragment();
        }
        return fragment;
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        PreferenceScreen root = getPreferenceManager().createPreferenceScreen(getActivity());
        root.setTitle("Select a report to view");

        Preference totalSales = new Preference(getActivity());
        totalSales.setTitle("Total Sales");
        totalSales.setSummary("Total sales since started using app on"/* Add first item added date */);
        totalSales.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                return true;
            }
        });
        root.addPreference(totalSales);

        Preference salesByDay = new Preference(getActivity());
        salesByDay.setTitle("Daily Inventory/Sales");
        salesByDay.setSummary("Daily sales up to yesterday"/* Add yesterday's date */);
        root.addPreference(salesByDay);

        Preference salesByMonth = new Preference(getActivity());
        salesByMonth.setTitle("Monthly Inventory/Sales");
        salesByMonth.setSummary("Monthly sales up to last month"/* Add the last day of the last month */);
        root.addPreference(salesByMonth);

        Preference salesByYear = new Preference(getActivity());
        salesByYear.setTitle("Annual Inventory/Sales");
        salesByYear.setSummary("Annual sales up to the end of last year"/* Add the last year */);
        root.addPreference(salesByYear);

        setPreferenceScreen(root);
    }
}
