package com.tsadev.inventorymanagement;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class ItemAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<Item> itemList;

    public ItemAdapter(Context context, List<Item> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return null;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,boolean isLastChild, View convertView, ViewGroup parent) {
        final Item item = itemList.get(groupPosition);
        ChildHolder holder;
        if (convertView == null) {
            holder = new ChildHolder();
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_row_item_expanded, parent, false);
            holder.picture = (ImageView)convertView.findViewById(R.id.item_picture);
            holder.desc = (TextView)convertView.findViewById(R.id.item_description);
            holder.category = (TextView)convertView.findViewById(R.id.item_category);
            holder.priceEach = (TextView)convertView.findViewById(R.id.item_price_each);
            holder.priceAll = (TextView)convertView.findViewById(R.id.item_price_all);
            holder.edit = (RelativeLayout)convertView.findViewById(R.id.edit_item);
            convertView.setTag(holder);
        } else {
            holder = (ChildHolder)convertView.getTag();
        }
        String desc = item.getDesc(),
                cat = item.getCategory(),
                priceEach = item.getPrice(),
                priceAll = item.getPriceAll();
        if (desc != null && !desc.isEmpty()) holder.desc.setText(item.getDesc()); else holder.desc.setText("None set");
        if (cat != null && !cat.isEmpty()) holder.category.setText(item.getCategory()); else holder.category.setText("None");
        if (priceEach != null && !priceEach.isEmpty()) holder.priceEach.setText(item.getPrice()); else holder.priceEach.setText("$0.00");
        if (priceAll != null && !priceAll.isEmpty()) holder.priceAll.setText(item.getPriceAll()); else holder.priceAll.setText("$0.00");
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent edit = new Intent(Vars.ACTION_EDIT);
                edit.putExtra("item", item);
                context.sendBroadcast(edit);
            }
        });
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return itemList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return itemList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        Item item = itemList.get(groupPosition);
        GroupHolder holder;
        if (convertView == null) {
            holder = new GroupHolder();
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_row_item, parent, false);
            holder.name = (TextView)convertView.findViewById(R.id.item_title);
            holder.color = (ImageView)convertView.findViewById(R.id.item_color);
            holder.count = (TextView)convertView.findViewById(R.id.item_subtitle);
            convertView.setTag(holder);
        } else {
            holder = (GroupHolder) convertView.getTag();
        }
        holder.name.setText(item.getName());
        holder.count.setText(String.valueOf(item.getCount()));
        String itemColor = item.getColor();
        holder.color.setColorFilter(0);
        if (itemColor != null && !itemColor.isEmpty()) {
            try {
                int color = Color.parseColor(itemColor);
                holder.color.setColorFilter(color);
            } catch (IllegalArgumentException e) {
                Log.e("TSDEBUG", "Couldn't parse color");
            }
        } else {
            holder.color.setColorFilter(0xFFcfcfcf);
        }


        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    private class GroupHolder {
        ImageView color;
        TextView name,count;
    }

    private class ChildHolder {
        ImageView picture;
        TextView desc, category, priceEach, priceAll;
        RelativeLayout edit;
    }
}
