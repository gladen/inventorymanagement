package com.tsadev.inventorymanagement;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.LinearLayout;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

public class GraphFragment extends Fragment{


    private View createLayout() {

        GraphView graphView = new GraphView(getActivity());
        graphView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 1));
        graphView.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    // show normal x values
                    return super.formatLabel(value, isValueX);
                } else {
                    // show currency for y values
                    return "$" + super.formatLabel(value, isValueX);
                }
            }
        });
        graphView.setTitle("Items Sold This Week");
        double dollars = 10.00;
        LineGraphSeries<DataPoint> data = new LineGraphSeries<>(new DataPoint[]{
                new DataPoint(0, dollars),
                new DataPoint(1, dollars + 10),
                new DataPoint(2, dollars + 50),
                new DataPoint(3, dollars + 1000)
        });
        graphView.addSeries(data);
        data.setTitle("Amount in $$");
        graphView.getLegendRenderer().setVisible(true);
        graphView.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);
        return graphView;
    }
}
