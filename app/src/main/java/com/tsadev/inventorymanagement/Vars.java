package com.tsadev.inventorymanagement;

public class Vars {

    private static Vars vars;
    private static final String PACKAGE_NAME = "com.tsadev.inventorymanagement";

    boolean useExternalMode, useColors, firstRun, eulaAccepted, licensed;
    String dateFormat, version;

    static final String ACTION_RESTART = PACKAGE_NAME + ".Restart";
    static final String ACTION_EDIT = PACKAGE_NAME + ".Edit";
    static final boolean DEBUG = true, CHECK_LICENSE = false;
    static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAg8Xy3P3Hx"
            + "s1SoDJdWGRTkuimotCYVZmp7CoUKvvQRHXnspPHnl+ShLLn9QRX47Uc0et8rdqqPCL5lqYA6sAo/Lcn3zIObc"
            + "qtl5bQbN3ZJcih/M2BKtbMZOcNiSRXMSnCHAURPc4bYPYidRCtMX7g31WARfsCmT+AbKwrqnilz53lhDBgFT2"
            + "KB3cr1FFwa6G2PrelnKXn873gEfEbiNmJtvH8wHQbVysdUl2xdITU8Gfx40/vei8oJqGUO++/ciPjnW6fSOmw"
            +  "7z8LyxO16TT8uVUcPu5Bz09prHM20Vp7KVvO8mIqPldhLmJ1ZCS1rrejWLAbxjqvEGzz1YCbfG5qcwIDAQAB";

    static final byte[] SALT = new byte[] {50, 24, 63, 86, 91, 49, 19, 32, 68, 15, 48, 24,
            95, 39, 17, 68, 38, 87, 41, 81};

    static Vars getInstance() {
        if (vars == null) {
            synchronized (Vars.class) {
                if (vars == null) {
                    vars = new Vars();
                }
            }
        }
        return vars;
    }

    private Vars() {

    }
}
